# MeetChat

### Features
- No account needed ( Need to add auth )
- Messaging chat and video streaming in real-time
- Screen sharing to present documents, slides, and more
- Everyting is peer-to-peer (using mesh method)
- smooth UI and UX setup
- more to be added soon

### Local setup

1. `yarn install`
2. `yarn dev`
3. `go to /home for frontend`
4. `/room for chatrooms`
