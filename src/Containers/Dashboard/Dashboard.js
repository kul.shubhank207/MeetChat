import React from 'react'
import Header from '../../Components/Header/Header';
import Sidebar from '../../Components/SideBar/Sidebar'
import Home from '../MeetStart/Home';

function Dashboard() {
    return (
        <div>
            <Header />
            {/* <Sidebar /> */}
            <Home />
        </div>
    )
}

export default Dashboard
